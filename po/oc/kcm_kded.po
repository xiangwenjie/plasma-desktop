# translation of kcmkded.po to Occitan (lengadocian)
# Occitan translation of kcmkded
# Copyright (C) 2002,2003, 2004, 2007, 2008 Free Software Foundation, Inc.
#
# Yannig MARCHEGAY (Kokoyaya) <yannig@marchegay.org> - 2006-2007
#
# Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>, 2007, 2008.
msgid ""
msgstr ""
"Project-Id-Version: kcmkded\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-05-28 00:39+0000\n"
"PO-Revision-Date: 2008-08-05 22:26+0200\n"
"Last-Translator: Yannig Marchegay (Kokoyaya) <yannig@marchegay.org>\n"
"Language-Team: Occitan (lengadocian) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: KBabel 1.11.4\n"

#: kcmkded.cpp:115
#, kde-format
msgid "Failed to stop service: %1"
msgstr ""

#: kcmkded.cpp:117
#, kde-format
msgid "Failed to start service: %1"
msgstr ""

#: kcmkded.cpp:124
#, kde-format
msgid "Failed to stop service."
msgstr ""

#: kcmkded.cpp:126
#, kde-format
msgid "Failed to start service."
msgstr ""

#: kcmkded.cpp:224
#, kde-format
msgid "Failed to notify KDE Service Manager (kded6) of saved changed: %1"
msgstr ""

#: ui/main.qml:40
#, kde-format
msgid ""
"The background services manager (kded6) is currently not running. Make sure "
"it is installed correctly."
msgstr ""

#: ui/main.qml:50
#, kde-format
msgid ""
"Some services disable themselves again when manually started if they are not "
"useful in the current environment."
msgstr ""

#: ui/main.qml:60
#, kde-format
msgid ""
"Some services were automatically started/stopped when the background "
"services manager (kded6) was restarted to apply your changes."
msgstr ""

#: ui/main.qml:108
#, fuzzy, kde-format
#| msgid "Service"
msgid "All Services"
msgstr "Servici"

#: ui/main.qml:109
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List running services"
msgid "Running"
msgstr "En foncionament"

#: ui/main.qml:110
#, fuzzy, kde-format
#| msgid "Running"
msgctxt "List not running services"
msgid "Not Running"
msgstr "En foncionament"

#: ui/main.qml:147
#, fuzzy, kde-format
#| msgid "Service"
msgid "Startup Services"
msgstr "Servici"

#: ui/main.qml:148
#, kde-format
msgid "Load-on-Demand Services"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Disable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:167
#, kde-format
msgctxt "@action:button %1 service name"
msgid "Enable automatically loading %1 on startup"
msgstr ""

#: ui/main.qml:168
#, kde-format
msgid "Toggle automatically loading this service on startup"
msgstr ""

#: ui/main.qml:212
#, fuzzy, kde-format
#| msgid "Running"
msgid "Not running"
msgstr "En foncionament"

#: ui/main.qml:213
#, kde-format
msgid "Running"
msgstr "En foncionament"

#: ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Stop"
msgctxt "@action:button %1 service name"
msgid "Stop %1"
msgstr "Arrestar"

#: ui/main.qml:233
#, fuzzy, kde-format
#| msgid "Start"
msgctxt "@action:button %1 service name"
msgid "Start %1"
msgstr "Aviar"

#: ui/main.qml:236
#, fuzzy, kde-format
#| msgid "Service"
msgid "Stop Service"
msgstr "Servici"

#: ui/main.qml:236
#, fuzzy, kde-format
#| msgid "Service"
msgid "Start Service"
msgstr "Servici"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Yannig Marchegay (Kokoyaya)"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "yannig@marchegay.org"

#~ msgid "Daniel Molkentin"
#~ msgstr "Daniel Molkentin"

#~ msgid "kcmkded"
#~ msgstr "kcmkded"

#~ msgid "Status"
#~ msgstr "Estatut"

#~ msgid "Description"
#~ msgstr "Descripcion"
