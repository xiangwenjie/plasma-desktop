# Copyright (C) 2023 This file is copyright:
# This file is distributed under the same license as the plasma-desktop package.
#
# SPDX-FileCopyrightText: 2023, 2024 Enol P. <enolp@softastur.org>
msgid ""
msgstr ""
"Project-Id-Version: plasma-desktop\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2024-06-09 00:40+0000\n"
"PO-Revision-Date: 2024-03-18 23:42+0100\n"
"Last-Translator: Enol P. <enolp@softastur.org>\n"
"Language-Team: Asturian <alministradores@softastur.org>\n"
"Language: ast\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.02.0\n"

#: actions.cpp:19
#, kde-format
msgid "Touchpad"
msgstr "Panel táctil"

#: actions.cpp:22
#, kde-format
msgid "Enable Touchpad"
msgstr ""

#: actions.cpp:30
#, kde-format
msgid "Disable Touchpad"
msgstr ""

#: actions.cpp:38
#, kde-format
msgid "Toggle Touchpad"
msgstr ""

#: backends/kwin_wayland/kwinwaylandbackend.cpp:59
#, kde-format
msgid "Querying input devices failed. Please reopen this settings module."
msgstr ""

#: backends/kwin_wayland/kwinwaylandbackend.cpp:74
#, kde-format
msgid "Critical error on reading fundamental device infos for touchpad %1."
msgstr ""

#: backends/x11/xlibbackend.cpp:71
#, kde-format
msgid "Cannot connect to X server"
msgstr ""

#: backends/x11/xlibbackend.cpp:84 ui/main.qml:98
#, kde-format
msgid "No touchpad found"
msgstr "Nun s'atopó'l panel táctil"

#: backends/x11/xlibbackend.cpp:124 backends/x11/xlibbackend.cpp:138
#, kde-format
msgid "Cannot apply touchpad configuration"
msgstr "Nun se pue aplicar l'aplicación del panel táctil"

#: backends/x11/xlibbackend.cpp:152 backends/x11/xlibbackend.cpp:165
#, kde-format
msgid "Cannot read touchpad configuration"
msgstr "Nun se pue lleer la configuración de la pantalla táctil"

#: backends/x11/xlibbackend.cpp:178
#, kde-format
msgid "Cannot read default touchpad configuration"
msgstr "Nun se pue lleer la configuración predeterminada del panel táctil"

#: kcm.cpp:99
#, kde-format
msgid ""
"Error while loading values. See logs for more information. Please restart "
"this configuration module."
msgstr ""

#: kcm.cpp:102
#, kde-format
msgid "No touchpad found. Connect touchpad now."
msgstr "Nun s'atopó nengún panel táctil. Conecta unu agora."

#: kcm.cpp:111
#, kde-format
msgid ""
"Not able to save all changes. See logs for more information. Please restart "
"this configuration module and try again."
msgstr ""

#: kcm.cpp:130
#, kde-format
msgid ""
"Error while loading default values. Failed to set some options to their "
"default values."
msgstr ""

#: kcm.cpp:150
#, kde-format
msgid ""
"Error while adding newly connected device. Please reconnect it and restart "
"this configuration module."
msgstr ""

#: kcm.cpp:173
#, kde-format
msgid "Touchpad disconnected. Closed its setting dialog."
msgstr ""

#: kcm.cpp:175
#, kde-format
msgid "Touchpad disconnected. No other touchpads found."
msgstr ""

#: kded/kded.cpp:201
#, kde-format
msgid "Touchpad was disabled because a mouse was plugged in"
msgstr ""

#: kded/kded.cpp:204
#, kde-format
msgid "Touchpad was enabled because the mouse was unplugged"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:27
#, kde-format
msgctxt "Emulated mouse button"
msgid "No action"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:30
#, kde-format
msgctxt "Emulated mouse button"
msgid "Left button"
msgstr "Botón esquierdu"

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:33
#, kde-format
msgctxt "Emulated mouse button"
msgid "Middle button"
msgstr ""

#. i18n: ectx: label, entry ($(TapAction)Button), group (parameters)
#: touchpadparameters.kcfg:36
#, kde-format
msgctxt "Emulated mouse button"
msgid "Right button"
msgstr "Botón derechu"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:285
#, kde-format
msgctxt "Touchpad Edge"
msgid "All edges"
msgstr "Tolos berbesos"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:288
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top edge"
msgstr "Berbesu cimeru"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:291
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top right corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:294
#, kde-format
msgctxt "Touchpad Edge"
msgid "Right edge"
msgstr "Berbesu derechu"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:297
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom right corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:300
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom edge"
msgstr "Berbesu baxeru"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:303
#, kde-format
msgctxt "Touchpad Edge"
msgid "Bottom left corner"
msgstr ""

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:306
#, kde-format
msgctxt "Touchpad Edge"
msgid "Left edge"
msgstr "Berbesu esquierdu"

#. i18n: ectx: label, entry (CircScrollTrigger), group (parameters)
#: touchpadparameters.kcfg:309
#, kde-format
msgctxt "Touchpad Edge"
msgid "Top left corner"
msgstr ""

#: ui/main.qml:99
#, kde-format
msgid "Connect an external touchpad"
msgstr ""

#: ui/main.qml:113
#, kde-format
msgid "Device:"
msgstr "Preséu:"

#: ui/main.qml:139
#, kde-format
msgid "General:"
msgstr "Xeneral:"

#: ui/main.qml:140
#, kde-format
msgid "Device enabled"
msgstr "Preséu activáu"

#: ui/main.qml:144
#, kde-format
msgid "Accept input through this device."
msgstr ""

#: ui/main.qml:168
#, kde-format
msgid "Disable while typing"
msgstr ""

#: ui/main.qml:172
#, kde-format
msgid "Disable touchpad while typing to prevent accidental inputs."
msgstr ""

#: ui/main.qml:196
#, fuzzy, kde-format
#| msgctxt "Touchpad Edge"
#| msgid "Left edge"
msgid "Left handed mode"
msgstr "Berbesu esquierdu"

#: ui/main.qml:200
#, kde-format
msgid "Swap left and right buttons."
msgstr ""

#: ui/main.qml:226
#, kde-format
msgid "Press left and right buttons for middle click"
msgstr ""

#: ui/main.qml:230 ui/main.qml:874
#, kde-format
msgid ""
"Clicking left and right button simultaneously sends middle button click."
msgstr ""

#: ui/main.qml:256
#, kde-format
msgid ""
"Activating this setting increases mouse click latency by 50ms. The extra "
"delay is needed to correctly detect simultaneous left and right mouse clicks."
msgstr ""

#: ui/main.qml:266
#, kde-format
msgid "Pointer speed:"
msgstr ""

#: ui/main.qml:364
#, kde-format
msgid "Pointer acceleration:"
msgstr ""

#: ui/main.qml:395
#, kde-format
msgid "None"
msgstr ""

#: ui/main.qml:399
#, kde-format
msgid "Cursor moves the same distance as finger."
msgstr ""

#: ui/main.qml:408
#, kde-format
msgid "Standard"
msgstr "Estándar"

#: ui/main.qml:412
#, kde-format
msgid "Cursor travel distance depends on movement speed of finger."
msgstr ""

#: ui/main.qml:426
#, kde-format
msgid "Scrolling speed:"
msgstr ""

#: ui/main.qml:477
#, kde-format
msgctxt "Slower Scroll"
msgid "Slower"
msgstr ""

#: ui/main.qml:484
#, kde-format
msgctxt "Faster Scroll Speed"
msgid "Faster"
msgstr ""

#: ui/main.qml:492
#, kde-format
msgid "Scrolling:"
msgstr ""

#: ui/main.qml:521
#, kde-format
msgid "Two fingers"
msgstr ""

#: ui/main.qml:525
#, kde-format
msgid "Slide with two fingers scrolls."
msgstr ""

#: ui/main.qml:533
#, fuzzy, kde-format
#| msgid "Touchpad"
msgid "Touchpad edges"
msgstr "Panel táctil"

#: ui/main.qml:537
#, kde-format
msgid "Slide on the touchpad edges scrolls."
msgstr ""

#: ui/main.qml:547
#, kde-format
msgid "Invert scroll direction (Natural scrolling)"
msgstr ""

#: ui/main.qml:563
#, kde-format
msgid "Touchscreen like scrolling."
msgstr ""

#: ui/main.qml:571 ui/main.qml:588
#, kde-format
msgid "Disable horizontal scrolling"
msgstr ""

#: ui/main.qml:601
#, kde-format
msgid "Tapping:"
msgstr ""

#: ui/main.qml:602
#, kde-format
msgid "Tap-to-click"
msgstr ""

#: ui/main.qml:606
#, kde-format
msgid "Single tap is left button click."
msgstr ""

#: ui/main.qml:635
#, kde-format
msgid "Tap-and-drag"
msgstr ""

#: ui/main.qml:639
#, kde-format
msgid "Sliding over touchpad directly after tap drags."
msgstr ""

#: ui/main.qml:666
#, kde-format
msgid "Tap-and-drag lock"
msgstr ""

#: ui/main.qml:670
#, kde-format
msgid "Dragging continues after a short finger lift."
msgstr ""

#: ui/main.qml:690
#, kde-format
msgid "Two-finger tap:"
msgstr ""

#: ui/main.qml:701
#, kde-format
msgid "Right-click (three-finger tap to middle-click)"
msgstr ""

#: ui/main.qml:702
#, kde-format
msgid ""
"Tap with two fingers to right-click, tap with three fingers to middle-click."
msgstr ""

#: ui/main.qml:704
#, kde-format
msgid "Middle-click (three-finger tap right-click)"
msgstr ""

#: ui/main.qml:705
#, kde-format
msgid ""
"Tap with two fingers to middle-click, tap with three fingers to right-click."
msgstr ""

#: ui/main.qml:707
#, kde-format
msgid "Right-click"
msgstr ""

#: ui/main.qml:708
#, kde-format
msgid "Tap with two fingers to right-click."
msgstr ""

#: ui/main.qml:710
#, kde-format
msgid "Middle-click"
msgstr ""

#: ui/main.qml:711
#, kde-format
msgid "Tap with two fingers to middle-click."
msgstr ""

#: ui/main.qml:769
#, kde-format
msgid "Right-click:"
msgstr ""

#: ui/main.qml:803
#, kde-format
msgid "Press bottom-right corner"
msgstr ""

#: ui/main.qml:807
#, kde-format
msgid ""
"Software enabled buttons will be added to bottom portion of your touchpad."
msgstr ""

#: ui/main.qml:815
#, kde-format
msgid "Press anywhere with two fingers"
msgstr ""

#: ui/main.qml:819
#, kde-format
msgid "Tap with two finger to enable right click."
msgstr ""

#: ui/main.qml:829
#, kde-format
msgid "Middle-click: "
msgstr ""

#: ui/main.qml:858
#, kde-format
msgid "Press bottom-middle"
msgstr ""

#: ui/main.qml:862
#, kde-format
msgid ""
"Software enabled middle-button will be added to bottom portion of your "
"touchpad."
msgstr ""

#: ui/main.qml:870
#, kde-format
msgid "Press bottom left and bottom right corners simultaneously"
msgstr ""

#: ui/main.qml:883
#, kde-format
msgid "Press anywhere with three fingers"
msgstr ""

#: ui/main.qml:889
#, kde-format
msgid "Press anywhere with three fingers."
msgstr ""

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Softastur"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "alministradores@softastur.org"
